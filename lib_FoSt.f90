MODULE FoST
   ! Fortran String Transformations
   USE PorPre,       ONLY: vsi,  si,  di,  qi,  sr,  dr,  qr,  sc,  dc,  qc, &
                           & Dvsi, Dsi, Ddi, Dqi, Dsr, Ddr, Dqr, Dsc, Ddc, Dqc, strlen, ds

   interface stringify
      module procedure stringify_vsi, stringify_si,  stringify_di,  stringify_qi, &
                     & stringify_sr, stringify_dr, &
#ifndef __PGI
                     ! This tells the preprocessor to define this module only if we don't use the PGI Compiler
                     ! Either way, the compiler would get lost between dr and qr as they have the same value.
                     & stringify_qr, &
#endif
                     & stringify_sc, stringify_dc, stringify_qc, &
                     & stringify_lg
   endinterface

   interface str2int
      module procedure str2int_vsi, str2int_si, str2int_di, str2int_qi
   endinterface

   interface str2real
      module procedure str2real_sr, &
#ifndef __PGI
                     ! This tells the preprocessor to define this module only if we don't use the PGI Compiler
                     ! Either way, the compiler would get lost between dr and qr as they have the same value.
                     & str2real_qr, &
#endif
                     & str2real_dr
   endinterface

   type string_t
      character (len=:), allocatable :: str
   end type string_t

   public :: stringify
   public :: string_t
   public :: str2int, str2real, str2logi
   public :: Upper, Lower

   private

contains
!!!!! stringify !!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Stringify might be put in a string_utils module in the futur if useful

   elemental function stringify_vsi(args) result(string)
      implicit none
      integer(kind=vsi), intent(in) :: args
      character(len=Dvsi)            :: string
      !character(len=:), allocatable :: string
      !!character(len=digits(args)) :: tmp

      write(string, *) args
      !write(tmp, *) args
      !string=trim(adjustl(tmp))
      return
   endfunction stringify_vsi

   ! elemental function stringify_i(args) result(string)
      ! implicit none
      ! integer, intent(in) :: args
      ! character(len=Ddi)            :: string
      ! !character(len=:), allocatable :: string
      ! !!character(len=digits(args)) :: tmp

      ! write(string, *) args
      ! !write(tmp, *) args
      ! !string=trim(adjustl(tmp))
      ! return
   ! endfunction stringify_i

   elemental function stringify_si(args) result(string)
      implicit none
      integer(kind=si), intent(in) :: args
      character(len=Dsi)            :: string
      ! character(len=:), allocatable :: string
      ! character(len=digits(args)) :: tmp

      write(string, *) args
      ! write(tmp, *) args
      ! string=trim(adjustl(tmp))
      return
   endfunction stringify_si

   elemental function stringify_di(args) result(string)
      implicit none
      integer(kind=di), intent(in) :: args
      character(len=Ddi)            :: string
      ! character(len=:), allocatable :: string
      ! character(len=digits(args)) :: tmp

      write(string, *) args
      ! write(tmp, *) args
      ! string=trim(adjustl(tmp))
      return
   endfunction stringify_di

   elemental function stringify_qi(args) result(string)
      implicit none
      integer(kind=qi), intent(in) :: args
      character(len=Dqi)            :: string
      ! character(len=:), allocatable :: string
      ! character(len=digits(args)) :: tmp

      write(string, *) args
      ! write(tmp, *) args
      ! string=trim(adjustl(tmp))
      return
   endfunction stringify_qi

   elemental function stringify_sr(args) result(string)
      implicit none
      real(kind=sr), intent(in) :: args
      character(len=Dsr)            :: string
      ! character(len=:), allocatable :: string
      ! character(len=digits(args)) :: tmp

      write(string, *) args
      ! write(tmp, *) args
      ! string=trim(adjustl(tmp))
      return
   endfunction stringify_sr

   elemental function stringify_dr(args) result(string)
      implicit none
      real(kind=dr), intent(in) :: args
      character(len=Ddr)            :: string
      ! character(len=:), allocatable :: string
      ! character(len=digits(args)) :: tmp

      write(string, *) args
      ! write(tmp, *) args
      ! string=trim(adjustl(tmp))
      return
   endfunction stringify_dr

   elemental function stringify_qr(args) result(string)
      implicit none
      real(kind=qr), intent(in) :: args
      character(len=Dqr)            :: string
      ! character(len=:), allocatable :: string
      ! character(len=digits(args)) :: tmp

      write(string, *) args
      ! write(tmp, *) args
      ! string=trim(adjustl(tmp))
      return
   endfunction stringify_qr

   elemental function stringify_sc(args) result(string)
      implicit none
      complex(kind=sc), intent(in) :: args
      character(len=Dqc)            :: string
      ! character(len=:), allocatable :: string
      ! character(len=46) :: tmp

      write(string, *) args
      ! write(tmp, *) args
      ! string=trim(adjustl(tmp))
      return
   endfunction stringify_sc

   elemental function stringify_dc(args) result(string)
      implicit none
      complex(kind=dc), intent(in) :: args
      character(len=Ddc)            :: string
      ! character(len=:), allocatable :: string
      ! character(len=315) :: tmp

      write(string, *) args
      ! write(tmp, *) args
      ! string=trim(adjustl(tmp))
      return
   endfunction stringify_dc

   elemental function stringify_qc(args) result(string)
      implicit none
      complex(kind=qc), intent(in) :: args
      character(len=Dqc)            :: string
      ! character(len=:), allocatable :: string
      ! character(len=4931+7+1) :: tmp

      write(string, *) args
      ! write(tmp, *) args
      ! string=trim(adjustl(tmp))
      return
   endfunction stringify_qc

   elemental function stringify_lg(args) result(string)
      implicit none
      logical, intent(in) :: args
      character(len=8)            :: string
      ! character(len=:), allocatable :: string
      ! character(len=8) :: tmp

      write(string, *) args
      ! write(tmp, *) args
      ! string=trim(adjustl(tmp))
      return
   endfunction stringify_lg
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!!!!! str to nb !!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

   ELEMENTAL subroutine str2int_vsi(str,int)! ,stat)
   ! ---------------------------------
   !
   ! ---------------------------------
      implicit none
      ! Arguments
      character(len=*), intent(in) :: str
      integer (kind=vsi),intent(out)         :: int
      ! integer,intent(out)         :: stat
      ! ---------------------------------

      ! read(str,*,iostat=stat)  int
      read(str,*)  int
   end subroutine str2int_vsi

   ELEMENTAL subroutine str2int_si(str,int)! ,stat)
   ! ---------------------------------
   !
   ! ---------------------------------
      implicit none
      ! Arguments
      character(len=*), intent(in) :: str
      integer (kind=si),intent(out)         :: int
      ! integer,intent(out)         :: stat
      ! ---------------------------------

      ! read(str,*,iostat=stat)  int
      read(str,*)  int
   end subroutine str2int_si

   ELEMENTAL subroutine str2int_di(str,int)! ,stat)
   ! ---------------------------------
   !
   ! ---------------------------------
      implicit none
      ! Arguments
      character(len=*), intent(in) :: str
      integer (kind=di),intent(out)         :: int
      ! integer,intent(out)         :: stat
      ! ---------------------------------

      ! read(str,*,iostat=stat)  int
      read(str,*)  int
   end subroutine str2int_di

   ELEMENTAL subroutine str2int_qi(str,int)! ,stat)
   ! ---------------------------------
   !
   ! ---------------------------------
      implicit none
      ! Arguments
      character(len=*), intent(in) :: str
      integer (kind=qi),intent(out)         :: int
      ! integer,intent(out)         :: stat
      ! ---------------------------------

      ! read(str,*,iostat=stat)  int
      read(str,*)  int
   end subroutine str2int_qi

   ELEMENTAL subroutine str2real_sr(str,real)! ,stat)
   ! ---------------------------------
   !
   ! ---------------------------------
      implicit none
      ! Arguments
      character(len=*),intent(in) :: str
      real (kind=sr), intent(out)         :: real
      ! integer,intent(out)         :: stat
      ! ---------------------------------

      ! read(str,*,iostat=stat)  int
      read(str,*)  real
   end subroutine str2real_sr

   ELEMENTAL subroutine str2real_dr(str,real)! ,stat)
   ! ---------------------------------
   !
   ! ---------------------------------
      implicit none
      ! Arguments
      character(len=*),intent(in) :: str
      real (kind=dr), intent(out)         :: real
      ! integer,intent(out)         :: stat
      ! ---------------------------------

      ! read(str,*,iostat=stat)  int
      read(str,*)  real
   end subroutine str2real_dr

   ELEMENTAL subroutine str2real_qr(str,real)! ,stat)
   ! ---------------------------------
   !
   ! ---------------------------------
      implicit none
      ! Arguments
      character(len=*),intent(in) :: str
      real (kind=qr), intent(out)         :: real
      ! integer,intent(out)         :: stat
      ! ---------------------------------

      ! read(str,*,iostat=stat)  int
      read(str,*)  real
   end subroutine str2real_qr


   ELEMENTAL subroutine str2logi(str,logi)! ,stat)
   ! ---------------------------------
   !
   ! ---------------------------------
      implicit none
      ! Arguments
      character(len=*),intent(in) :: str
      logical, intent(out)         :: logi
      ! integer,intent(out)         :: stat
      ! ---------------------------------

      ! read(str,*,iostat=stat)  int
      read(str,*)  logi
   end subroutine str2logi
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


!!!!! str convertion !!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ELEMENTAL function Upper(s1)  Result (s2)
   ! ---------------------------------
   !
   ! ---------------------------------
      character(*),intent(in)       :: s1
      character(len(s1)) :: s2
      character          :: ch
      integer,parameter  :: duc = ICHAR('A') - ICHAR('a')
      integer            :: i
      ! ---------------------------------

      do i = 1,LEN(s1)
         ch = s1(i:i)
         if (ch >= 'a'.AND.ch <= 'z') ch = CHAR(ICHAR(ch)+duc)
         s2(i:i) = ch
      end do
   end function Upper

   ! ---------------------------------
   ELEMENTAL function Lower(s1)  Result (s2)
   ! ---------------------------------
   !
   ! ---------------------------------
      character(*),intent(in)       :: s1
      character(len(s1)) :: s2
      character          :: ch
      integer,parameter  :: duc = ICHAR('A') - ICHAR('a')
      integer            :: i
      ! ---------------------------------

      do i = 1,LEN(s1)
         ch = s1(i:i)
         if (ch >= 'a'.AND.ch <= 'z') ch = CHAR(ICHAR(ch)-duc)
         s2(i:i) = ch
      end do
   end function Lower
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

endmodule FoSt
